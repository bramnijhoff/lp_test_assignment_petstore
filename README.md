**Frameworks**
-
- SerenityBDD (v2.3.12)
- Cucumber (v6)
- Restassured (v4.3.3)

**Installation**
-
1. Install Java
2. Install Git
3. Install IntelliJ
4. Install Maven
5. Clone the Git repository from GitLab link https://gitlab.com/bramnijhoff/lp_test_assignment_petstore.git to your local machine OR download as zip
6. Start a new IntelliJ project based on the cloned/downloaded Git project
7. In IntelliJ, open terminal, input command `mvn clean`, input command `mvn install`, input command `mvn clean veriy`
8. Verify that BUILD SUCCES is returned

**Run test**
-
`mvn clean verify`

**Write new test**
-
1. First write your BDD (cucumber) test in the correct feature file (spec) located in ./src/test/resources/features/
2. If the correct feature file doesn't exist, you create a new one
3. Write the "Glue code" in the correct step definition file located in ./src/test/com/petstore/cucumber/steps/
4. If the correct step file doesn't exist, you create a new one

**Reporting**
-
**Local report**
Copy the link that is generated at the end of BUILD SUCCES message.

**Possible future improvements**
-
- Add store + user api tests
- Add html report as artifact
- Add lombok for builder

**NOTES**
-
- Due to busy schedule last week I wasn't able to meet all the requirements that were requested in the assignment.
