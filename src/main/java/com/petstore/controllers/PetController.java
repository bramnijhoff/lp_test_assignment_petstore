package com.petstore.controllers;

import com.petstore.domain.Pet;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

public class PetController {
    public static String PET_ENDPOINT = "https://petstore.swagger.io/v2/pet";
    private RequestSpecification requestSpecification;

    // Restassured used to use RequestSpecification to reduce duplication
    public PetController() {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.log(LogDetail.ALL);
        requestSpecification = requestSpecBuilder.build();
    }

    // Restassured steps to create a new pet -- Body of pet is added in PetSteps.java
    public Pet addNewPet(Pet pet) {
         return given(requestSpecification)
                .body(pet)
                .post(PET_ENDPOINT).as(Pet.class);
    }

    // Restassured steps to find a pet based on ID
    public Pet findPet(Pet pet) {
        return given(requestSpecification)
                .pathParam("petId", pet.getId())
                .get(PET_ENDPOINT + "/{petId}").as(Pet.class);
    }

    // Restassured steps to update a pet
    public Pet updatePet(Pet pet) {
        return given(requestSpecification)
                .body(pet)
                .put(PET_ENDPOINT).as(Pet.class);
    }

    // Restassured steps to delete a pet
    public void deletePet(Pet pet) {
        given(requestSpecification)
                .pathParam("petId", pet.getId())
                .delete(PET_ENDPOINT + "/{petId}");
    }

    // Restassured steps to verify if pet is deleted
    public void verifyPetDeleted(Pet pet) {
        given(requestSpecification)
                .pathParam("petId", pet.getId())
                .get(PET_ENDPOINT + "/{petId}")
                .then()
                .body(containsString("Pet not found"));
    }
}