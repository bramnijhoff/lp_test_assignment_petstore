Feature: Everything about your Pets

Scenario: CRUD pet api
  Given I create a new pet
  When I verify new pet
  When I update the pet
  Then I verify updated pet
  When I delete the pet
  Then I verify if pet is deleted