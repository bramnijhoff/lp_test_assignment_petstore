package com.petstore.cucumber.steps;

import com.petstore.controllers.PetController;
import com.petstore.domain.Category;
import com.petstore.domain.Pet;
import com.petstore.domain.Status;
import com.petstore.domain.Tag;

import java.util.Collections;

import io.cucumber.java.Before;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Given;

import org.apache.commons.lang3.RandomStringUtils;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;

public class PetSteps {

	// Edit this to change photo, but leave it for now as it is a sint-bernard leaseplan photo
	private static final String PHOTO_URL = "https://mir-s3-cdn-cf.behance.net/project_modules/fs/0063a286784373.5da48cb24ce40.jpg";
	PetController petController;
	// Body creation for a new pet
	Pet pet = new Pet.Builder()
			.withId(RandomStringUtils.randomNumeric(10))
			.withName("Bernard-leaseplan")
			.withPhotoUrls(Collections.singletonList(PHOTO_URL))
			.withStatus(Status.available)
			.withTags(Collections.singletonList(new Tag(1, "sint-bernard")))
			.inCategory(new Category(1, "dogs")).build();

	@Before
	public void beforeClass() {
		petController = new PetController();
	}

	// Gherkin given step(s)
	@Given("^I create a new pet")
	public void createNewPet() {
		Pet petResponse = petController.addNewPet(pet);
		assertThat(petResponse, is(samePropertyValuesAs(pet)));
	}

	// Gherkin when step(s)
	@When("^I verify new pet")
	public void verifyNewPet() {
		Pet petResponse = petController.findPet(pet);
		assertThat(petResponse, is(samePropertyValuesAs(pet)));
	}

	@When("^I update the pet")
	public void updateNewPet() {
		pet.setName("Sint-Bernard-leaseplan");
		Pet petResponse = petController.updatePet(pet);
		assertThat(petResponse, is(samePropertyValuesAs(pet)));
	}

	@When("^I delete the pet")
	public void deleteNewPet() {
		petController.deletePet(pet);
	}

	// Gherkin then step(s)
	@Then("^I verify updated pet")
	public void verifyUpdatedPet() {
		Pet petResponse = petController.findPet(pet);
		assertThat(petResponse, is(samePropertyValuesAs(pet)));
	}

	@Then("^I verify if pet is deleted")
	public void checkDeletedPet() {
		petController.verifyPetDeleted(pet);
	}
}
